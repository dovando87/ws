package py.com.owl.owlspring.response;

import java.util.List;

public class ApiResponse<T> {

	private T data;
	private List<ApiFieldError> errorList;

	public T getData() {
		return data;
	}

	public void setData(T dato) {
		this.data = dato;
	}

	public List<ApiFieldError> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<ApiFieldError> errorList) {
		this.errorList = errorList;
	}

}

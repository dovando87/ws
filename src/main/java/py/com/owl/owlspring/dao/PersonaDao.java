package py.com.owl.owlspring.dao;

import java.util.List;

import py.com.owl.owlspring.domain.Persona;

public interface PersonaDao {

	void insert(Persona persona);

	void update(Persona persona);

	Persona find(Long id);

	void delete(Persona persona);

	List<Persona> getList();

	Persona find(String cedula);

}

package py.com.owl.owlspring.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.owlspring.dao.CargoDao;
import py.com.owl.owlspring.domain.Cargo;

@Repository
@SessionScope
public class CargoDaoImpl implements CargoDao {

	@Autowired
	private EntityManager em;

	@Override
	public void insert(Cargo cargo) {
		em.persist(cargo);
	}

	@Override
	public void update(Cargo cargo) {
		em.merge(cargo);
	}

	@Override
	public Cargo find(Long id) {
		return em.find(Cargo.class, id);
	}

	@Override
	public void delete(Cargo cargo) {
		em.remove(em.contains(cargo) ? cargo : em.merge(cargo));
	}

	@Override
	public List<Cargo> getList() {
		String jpql = "SELECT object(C) FROM Cargo AS C";
		Query query = em.createQuery(jpql);
		return query.getResultList();
	}

	@Override
	public Cargo find(String nombre) {
		String jpql = "select object(C) from Cargo as c where nombre=?1 ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, nombre);
		try {
			return (Cargo) query.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

}

package py.com.owl.owlspring.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.context.annotation.SessionScope;

import py.com.owl.owlspring.dao.PersonaDao;
import py.com.owl.owlspring.domain.Persona;
import py.com.owl.owlspring.util.Log;

@Repository
@SessionScope
public class PersonaDaoImpl implements PersonaDao {

	private Log log = new Log(getClass());

	@Autowired
	private EntityManager em;

	@Override
	public Persona find(Long id) {

		return em.find(Persona.class, id);

	}

	@Override
	public void insert(Persona persona) {

		em.persist(persona);
	}

	@Override
	public void delete(Persona persona) {

		// em.remove(persona);
		em.remove(em.contains(persona) ? persona : em.merge(persona));

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Persona> getList() {

		String jpql = "SELECT object(P) FROM Persona AS P";
		Query query = em.createQuery(jpql);
		return query.getResultList();
	}

	@Override
	public Persona find(String cedula) {
		String jpql = "select object(P) from Persona as p where TRIM(cedula)=TRIM(?1) ";
		Query query = em.createQuery(jpql);
		query.setParameter(1, cedula);
		try {
			return (Persona) query.getSingleResult();
		} catch (NoResultException nre) {
			log.info("No se encontró {}", cedula);
			return null;
		}
	}

	@Override
	public void update(Persona persona) {
		em.merge(persona);
	}

}

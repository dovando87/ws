package py.com.owl.owlspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.owl.owlspring.domain.Cargo;
import py.com.owl.owlspring.response.ApiFieldError;
import py.com.owl.owlspring.response.ApiResponse;
import py.com.owl.owlspring.service.CargoService;
import py.com.owl.owlspring.util.Log;

@Controller
@RequestMapping("/cargo")
public class CargoController {

	private Log log = new Log(getClass());
	@Autowired
	private CargoService cargoService;

	@PostMapping()
	public ResponseEntity<ApiResponse<Cargo>> create(@Valid Cargo cargo, BindingResult bindingResult) {
		ApiResponse<Cargo> response = new ApiResponse<>();
		response.setData(cargo);

		if (bindingResult.hasErrors()) {
			List<ApiFieldError> errorList = new ArrayList<>();
			for (FieldError error : bindingResult.getFieldErrors()) {
				ApiFieldError apiFieldError = new ApiFieldError();
				apiFieldError.setName(error.getField());
				apiFieldError.setMessage(error.getDefaultMessage());
				errorList.add(apiFieldError);
			}
			response.setErrorList(errorList);
			return ResponseEntity.badRequest().body(response);
		} else {
			try {
				cargoService.insert(cargo);
				return new ResponseEntity<>(response, HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@PutMapping()
	public ResponseEntity<ApiResponse<Cargo>> update(@Valid Cargo cargo, BindingResult bindingResult) {
		ApiResponse<Cargo> response = new ApiResponse<>();
		response.setData(cargo);

		if (bindingResult.hasErrors()) {
			List<ApiFieldError> errorList = new ArrayList<>();
			for (FieldError error : bindingResult.getFieldErrors()) {
				ApiFieldError apiFieldError = new ApiFieldError();
				apiFieldError.setName(error.getField());
				apiFieldError.setMessage(error.getDefaultMessage());
				errorList.add(apiFieldError);
			}
			response.setErrorList(errorList);
			return ResponseEntity.badRequest().body(response);
		} else {
			try {
				cargoService.update(cargo);
				return new ResponseEntity<>(response, HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@DeleteMapping("/{id}") /* /cargo/1 o /cargo?id=1 con @RequestParam */
	public ResponseEntity<ApiResponse<Cargo>> delete(@PathVariable Long id) {
		ApiResponse<Cargo> response = new ApiResponse<Cargo>();
		try {
			Cargo bean = cargoService.find(id);
			if (bean == null) {
				return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
			}
			cargoService.delete(bean);
			response.setData(bean);
			return ResponseEntity.ok(response);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping()
	public ResponseEntity<ApiResponse<List<Cargo>>> getList() {
		ApiResponse<List<Cargo>> response = new ApiResponse<>();

		try {
			List<Cargo> list = cargoService.getList();
			response.setData(list);
			return ResponseEntity.ok(response);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/cedula/{id}")
	public ResponseEntity<ApiResponse<Cargo>> getbyCedula(@PathVariable String cedula) {
		ApiResponse<Cargo> response = new ApiResponse<Cargo>();
		try {
			Cargo bean = cargoService.find(cedula);
			if (bean == null) {
				return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
			}
			response.setData(bean);
			return ResponseEntity.ok(response);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

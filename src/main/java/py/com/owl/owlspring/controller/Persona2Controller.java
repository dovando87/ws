package py.com.owl.owlspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import py.com.owl.owlspring.domain.Persona;
import py.com.owl.owlspring.response.ApiFieldError;
import py.com.owl.owlspring.response.ApiResponse;
import py.com.owl.owlspring.service.PersonaService;
import py.com.owl.owlspring.util.Log;

@Controller
@RequestMapping("/persona2")
public class Persona2Controller {

	private Log log = new Log(getClass());
	@Autowired
	private PersonaService personaService;

	// @Autowired
	// private PersonaRepository personaRepository;

	@PostMapping()
	public ResponseEntity<ApiResponse<Persona>> create(@Valid Persona persona, BindingResult bindingResult) {
		ApiResponse<Persona> response = new ApiResponse<>();
		response.setData(persona);

		if (bindingResult.hasErrors()) {
			List<ApiFieldError> errorList = new ArrayList<>();
			for (FieldError error : bindingResult.getFieldErrors()) {
				ApiFieldError apiFieldError = new ApiFieldError();
				apiFieldError.setName(error.getField());
				apiFieldError.setMessage(error.getDefaultMessage());
				errorList.add(apiFieldError);
			}
			response.setErrorList(errorList);
			return ResponseEntity.badRequest().body(response);
		} else {
			try {
				personaService.insert(persona);
				return new ResponseEntity<>(response, HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@PutMapping()
	public ResponseEntity<ApiResponse<Persona>> update(@Valid Persona persona, BindingResult bindingResult) {
		ApiResponse<Persona> response = new ApiResponse<>();
		response.setData(persona);

		if (bindingResult.hasErrors()) {
			List<ApiFieldError> errorList = new ArrayList<>();
			for (FieldError error : bindingResult.getFieldErrors()) {
				ApiFieldError apiFieldError = new ApiFieldError();
				apiFieldError.setName(error.getField());
				apiFieldError.setMessage(error.getDefaultMessage());
				errorList.add(apiFieldError);
			}
			response.setErrorList(errorList);
			return ResponseEntity.badRequest().body(response);
		} else {
			try {
				personaService.update(persona);
				return new ResponseEntity<>(response, HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}

	@DeleteMapping("/{id}") /* /persona/1 o /persona?id=1 con @RequestParam */
	public ResponseEntity<ApiResponse<Persona>> delete(@PathVariable Long id) {
		ApiResponse<Persona> response = new ApiResponse<Persona>();
		try {
			Persona bean = personaService.find(id);
			if (bean == null) {
				return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
			}
			personaService.delete(bean);
			response.setData(bean);
			return ResponseEntity.ok(response);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping()
	public ResponseEntity<ApiResponse<List<Persona>>> getList() {
		ApiResponse<List<Persona>> response = new ApiResponse<>();

		try {
			List<Persona> list = personaService.getList();
			response.setData(list);
			return ResponseEntity.ok(response);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/cedula/{id}")
	public ResponseEntity<ApiResponse<Persona>> getbyCedula(@PathVariable String cedula) {
		ApiResponse<Persona> response = new ApiResponse<Persona>();
		try {
			Persona bean = personaService.find(cedula);
			if (bean == null) {
				return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
			}
			response.setData(bean);
			return ResponseEntity.ok(response);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * @GetMapping("/persona") public List<Persona> getList() { return
	 * personaRepository.findAll(); }
	 *
	 * @GetMapping("/{cedula}") public Persona buscarPorCedula(@PathVariable
	 * String cedula) { return personaRepository.findByCedula(cedula); }
	 */
}

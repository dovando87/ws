package py.com.owl.owlspring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.RequestScope;

import py.com.owl.owlspring.dao.PersonaDao;
import py.com.owl.owlspring.domain.Persona;
import py.com.owl.owlspring.service.PersonaService;

@Service
@RequestScope
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private PersonaDao personaDao;

	@Override
	@Transactional
	public void insert(Persona persona) {
		persona.setId(null);
		personaDao.insert(persona);

	}

	@Override
	@Transactional
	public Persona find(Long id) {
		return personaDao.find(id);
	}

	@Override
	@Transactional
	public void delete(Persona persona) {

		personaDao.delete(persona);

	}

	@Override
	@Transactional
	public List<Persona> getList() {

		return personaDao.getList();
	}

	@Override
	@Transactional
	public Persona find(String cedula) {
		return personaDao.find(cedula);
	}

	@Override
	@Transactional
	public void update(Persona persona) {
		personaDao.update(persona);

	}

}

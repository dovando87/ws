package py.com.owl.owlspring.service;

import java.util.List;

import py.com.owl.owlspring.domain.Persona;

public interface PersonaService {

	void insert(Persona persona);

	void update(Persona persona);

	Persona find(Long id);

	void delete(Persona persona);

	List<Persona> getList();

	Persona find(String cedula);
}

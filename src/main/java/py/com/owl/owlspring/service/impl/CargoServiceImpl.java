package py.com.owl.owlspring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import py.com.owl.owlspring.dao.CargoDao;
import py.com.owl.owlspring.domain.Cargo;
import py.com.owl.owlspring.service.CargoService;

@Service
@RequestScope
public class CargoServiceImpl implements CargoService {

	@Autowired
	private CargoDao cargoDao;

	@Override
	public void insert(Cargo cargo) {
		cargo.setId(null);
		cargoDao.insert(cargo);

	}

	@Override
	public void update(Cargo cargo) {
		cargoDao.update(cargo);
	}

	@Override
	public Cargo find(Long id) {
		return cargoDao.find(id);
	}

	@Override
	public void delete(Cargo cargo) {
		cargoDao.delete(cargo);

	}

	@Override
	public List<Cargo> getList() {
		return cargoDao.getList();
	}

	@Override
	public Cargo find(String nombre) {
		return cargoDao.find(nombre);
	}

}

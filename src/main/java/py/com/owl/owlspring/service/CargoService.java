package py.com.owl.owlspring.service;

import java.util.List;

import py.com.owl.owlspring.domain.Cargo;

public interface CargoService {

	void insert(Cargo cargo);

	void update(Cargo cargo);

	Cargo find(Long id);

	void delete(Cargo cargo);

	List<Cargo> getList();

	Cargo find(String nombre);

}
